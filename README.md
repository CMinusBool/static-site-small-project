# Cyril Szop - SZO0070 - SSP "Malá" práce

# Static Site Generator with CI

## Purpose
This project generates static HTML pages from Markdown files using continuous integration (CI) in GitLab.

## Usage
- Clone the repository.
- Add your Markdown files to the `content` directory.
- Commit and push changes to the repository.

## project requirements
* [x] GIT repozitář na Gitlabu.
  * [x] Repozitář má README.md (K čemu projekt slouží, jak se používá, jak se instaluje, jaké jsou prerekvizity, apod.)
* [x] Použití libovolného generátoru statických stránek dle vlastního výběru. (MKdocs, Middleman, Jekyll, apod.)
* [x] Vytvořená CI v repozitáři.
* [x] CI má minimálně dvě úlohy:
  * [x] Test kvality Markdown stránek.
  * [x] Generování HTML stránek z Markdown zdrojů.
* [x] CI má automatickou úlohou nasazení web stránek (deploy).
* [x] Gitlab projekt má fukční web stránky s generovaným obsahem na URL:
