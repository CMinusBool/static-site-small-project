# White Oak

## Description

The White Oak is another prominent species within the oak genus,
native to North America. It is valued for its strong and durable wood.

## Characteristics

- **Scientific Name**: Quercus alba
- **Height**: 15-24 meters
- **Crown Spread**: 15-24 meters
- **Hardiness Zones**: 3-9

## Uses

White Oaks are used in construction, shipbuilding,
and barrel making due to their sturdy wood.
They are also planted for their shade and ornamental value.
