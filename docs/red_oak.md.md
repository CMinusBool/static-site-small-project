# Red Oak

## Description

The Red Oak is a member of the oak genus (Quercus) and native to North America.
It is known for its beautiful red autumn foliage.

## Characteristics

- **Scientific Name**: Quercus rubra
- **Height**: 18-23 meters
- **Crown Spread**: 14-15 meters
- **Hardiness Zones**: 3-8

## Uses

Red Oaks are commonly used in landscaping for shade and aesthetic value.
Their wood is also prized for furniture and flooring.
